import { tableFromIPC } from "apache-arrow";
import { Criteria } from "./arrow_pb.js";
import { FlightServiceClient } from "./arrow_grpc_web_pb.js";

var client = new FlightServiceClient("http://localhost:8080");

$(document).ready(function () {
  $("#send-button").on("click", async function (e) {
    e.preventDefault();
    var request = new Criteria();
    var response = client.listFlights(request);
    console.log("response (var):")
    console.log(response);
    const table = await tableFromIPC(response);
    console.log(table.toArray());
  });
});
