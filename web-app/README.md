To build the JS web app you need:

1. arrow flight protobufs
2. grpc-web protobufs
3. package grpc-web with your custom javascript

### Download protoc and protoc-gen-grpc-web

Go to the `grpc/grpc-web` repo [download page](https://github.com/grpc/grpc-web/releases).

### Build protobufs for javascript

Assuming you're in `web-app` folder:

```shell
protoc -I=../protobufs ../protobufs/arrow.proto --js_out=import_style=commonjs:. --grpc-web_out=import_style=commonjs,mode=grpcwebtext:.
```

Note: There's a [known bug](https://github.com/protocolbuffers/protobuf-javascript/issues/127)!

On mac, the "easy" fix is installing with `brew`:

```shell
brew install protobuf@3
brew link --overwrite protobuf@3
```

### Build protobufs for grpc-web

```shell
protoc -I=../protobufs ../protobufs/arrow.proto --grpc-web_out=import_style=commonjs,mode=grpcwebtext:.
```

### Package the client.js

Finally use node to package client.js and dependencies in a single file importable from your html:

```shell
npm install
npx webpack
```

This will create a `public/js/flight-client.js` that you can embed (or serve) in your site.
