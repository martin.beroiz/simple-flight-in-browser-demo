### Envoy configuration

Envoy configuration file for gRPC-Web taken [from here](https://github.com/grpc/grpc-web/blob/master/net/grpc/gateway/examples/echo/envoy.yaml).

### Envoy Docker

Run the envoy proxy docker image

```shell
docker run -itd --name envoy-proxy --mount source=envoy.yaml,target=/etc/envoy/envoy.yaml envoyproxy/envoy:v1.21.6
```

After running, Envoy will be listening on port 8080, and it will display the admin interface on port 9901.
