# Apache Arrow Flight for the web Demo

## Run the demo with docker-compose

Launch all the services with docker-compose

```shell
docker-compose up
```

and test the app served at http://localhost:8010.
