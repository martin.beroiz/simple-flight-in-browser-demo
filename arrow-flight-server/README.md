# Apache Arrow-Flight server

A simple server to serve tables using apache arrow flight.
Use this instructions if you want to run this container in isolation.

### Build the container

```shell
docker build -t simple_flight:v1.0 .
```

### Run the image

```shell
docker run -tid --name simple_flight -p 31206:31206 simple_flight:v1.0
```

# Test the server

Test the Arrow Flight server using this python scripts

```shell
python test_client.py
```

You should see this output:

```
List Tables:
['animal_legs']
animal_legs table
pyarrow.Table
n_legs: int64
animals: string
----
n_legs: [[2,4,5,100]]
animals: [["Flamingo","Horse","Brittle stars","Centipede"]]
Adding bogus table: pyarrow.Table
index: int64
value: int64
----
index: [[1,2,3]]
value: [[0,0,0]]
List Tables:
['animal_legs', 'bogus']
bogus table from the server
pyarrow.Table
index: int64
value: int64
----
index: [[1,2,3]]
value: [[0,0,0]]
Dropping bogus table
List Tables:
['animal_legs']
```