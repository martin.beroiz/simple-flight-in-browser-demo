import sys
import logging
import argparse
import pyarrow as pa
import pyarrow.flight as flight

DEFAULT_HOST = "0.0.0.0"
DEFAULT_PORT = 31206

class FlightServer(flight.FlightServerBase):
    def __init__(self, host=DEFAULT_HOST, port=DEFAULT_PORT, **kwargs):
        self.location = flight.Location.for_grpc_tcp(host, port)
        self.logger = logging.getLogger()
        super().__init__(self.location, **kwargs)
        self.logger.debug(f"Server initiated at url: {host}:{port}.")
        # self.location = flight.Location.for_grpc_tcp(host, port)
        self.tables = {}
        n_legs = pa.array([2, 4, 5, 100])
        animals = pa.array(["Flamingo", "Horse", "Brittle stars", "Centipede"])
        self.tables[b"animal_legs"] = pa.table(
            [n_legs, animals], names=["n_legs", "animals"]
        )

    def do_put(self, context, descriptor, reader, writer):
        table_name = descriptor.command
        self.logger.debug(f"do_put: {table_name}")
        table = reader.read_all()
        self.tables[table_name] = table

    def do_get(self, context, ticket):
        table_name = ticket.ticket
        self.logger.debug(f"do_get: {table_name}")
        table = self.tables[table_name]
        return flight.RecordBatchStream(table)

    def flight_info(self, table_name, descriptor=None):
        table = self.tables[table_name]

        ticket = flight.Ticket(table_name)
        location = self.location.uri.decode("utf-8")
        endpoint = flight.FlightEndpoint(ticket, [location])

        descriptor = descriptor or flight.FlightDescriptor.for_command(table_name)
        return flight.FlightInfo(
            table.schema, descriptor, [endpoint], table.num_rows, table.nbytes
        )

    def get_flight_info(self, context, descriptor):
        self.logger.debug(f"get_flight_info: {descriptor.command}")
        return self.flight_info(descriptor.command, descriptor)

    def list_flights(self, context, criteria):
        self.logger.debug(f"list_flights")
        for table_name in self.tables.keys():
            yield self.flight_info(table_name)

    def do_action(self, context, action):
        if action.type == "drop_table":
            table_name = action.body.to_pybytes()
            del self.tables[table_name]
            self.logger.debug(f"drop_table: {table_name}")

        elif action.type == "shutdown":
            self.logger.debug("Shutting down")
            self.shutdown()

        else:
            raise KeyError("Unknown action {!r}".format(action.type))

    def list_actions(self, context):
        return [("drop_table", "Drop table"), ("shutdown", "Shut down server")]


def main():
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter("[%(asctime)s][%(name)s][%(levelname)s]: %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    parser = argparse.ArgumentParser(prog="simple-server")
    parser.add_argument(
        "-u",
        "--url",
        default=DEFAULT_HOST,
        help=f"Serve requests at this URL host. Defaults to {DEFAULT_HOST}",
    )
    parser.add_argument(
        "-p",
        "--port",
        default=DEFAULT_PORT,
        help=f"Serve requests at this URL port. Defaults to {DEFAULT_PORT}",
    )
    args = parser.parse_args()

    # serve requests
    server = FlightServer(host=args.url, port=args.port)
    logger.info(f"Starting Simple Server")
    server.serve()


if __name__ == "__main__":
    main()
