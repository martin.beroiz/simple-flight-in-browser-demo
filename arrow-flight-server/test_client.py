import pyarrow as pa
import pyarrow.flight as flight

class SimpleClient:
    def __init__(self, host="localhost", port=5678):
        self.location = flight.Location.for_grpc_tcp(host, port)
        self.connection = flight.connect(self.location)
        self.connection.wait_for_available()

    def put_table(self, name, table):
        table_name = name.encode("utf8")
        descriptor = flight.FlightDescriptor.for_command(table_name)
        writer, reader = self.connection.do_put(descriptor, table.schema)
        writer.write(table)
        writer.close()

    def get_table(self, name):
        table_name = name.encode("utf8")
        ticket = flight.Ticket(table_name)
        reader = self.connection.do_get(ticket)
        return reader.read_all()

    def list_tables(self):
        names = []
        for flight in self.connection.list_flights():
            table_name = flight.descriptor.command
            names.append(table_name.decode("utf-8"))
        return names

    def drop_table(self, name):
        table_name = name.encode("utf8")
        drop = flight.Action("drop_table", table_name)
        self.connection.do_action(drop)


if __name__ == "__main__":
    client = SimpleClient(port=31206)
    print("List Tables:")
    print(client.list_tables())
    print("animal_legs table")
    print(client.get_table("animal_legs"))

    bogus_list = [[1, 2, 3], [0, 0, 0]]
    bogus_table = pa.table(bogus_list, names=["index", "value"])
    print("Adding bogus table:", bogus_table)    
    client.put_table("bogus", bogus_table)

    print("List Tables:")
    print(client.list_tables())
    print("bogus table from the server")
    print(client.get_table("bogus"))

    print("Dropping bogus table")
    client.drop_table("bogus")
    print("List Tables:")
    print(client.list_tables())
